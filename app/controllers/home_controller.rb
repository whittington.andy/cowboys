class HomeController < ApplicationController
  def index
  end

  def name
    render json: { name: Faker::Name.name }
  end
end
