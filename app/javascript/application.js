// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "@hotwired/turbo-rails"
import "controllers"

import "custom/fabric.min"
import "custom/sprite.class"

var canvas = new fabric.Canvas('c');

fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center';
fabric.Object.prototype.transparentCorners = false;

fabric.Sprite.fromURL = function(url, callback, imgOptions) {
  fabric.util.loadImage(url, function(img) {
    callback(new fabric.Sprite(img, imgOptions));
  });
};

fabric.Sprite.fromURL('/assets/cowboy.png', createSprite(450,450),{name: 'cowboy',hitpoints: 500});
document.getElementById("healthbar").innerHTML = '500';
fabric.Sprite.fromURL('/assets/wing_demon.png', createSprite(200,200),{name: 'demon', hitpoints: 100});

function createSprite(x, y) {
    return function (sprite) {
	sprite.set({
	    left: x,
	    top: y,
	    angle: 0
	});
	canvas.add(sprite);
	setTimeout(function() {
	    sprite.set('dirty', true);
	    sprite.play();
	}, fabric.util.getRandomInt(1, 10) * 100);
    };
}

var pattern = new fabric.Pattern({
    source: '/assets/background.png',
    repeat: 'repeat',
});

canvas.backgroundColor = pattern;

// create a rectangle object
var rect = new fabric.Rect({
    fill: 'black',
    width: 4,
    height: 4,
    name: 'bullet'
});

async function GenerateHeroObjects ()
{
    // create a healthbar
    var healthbar = new fabric.Rect({
	fill: 'green',
	width: 50,
	height: 4,
	name: 'hero_healthbar',
	originX: 'center',
	originY: 'top'    
    });

    let randomName = await GetRandomName(); // get random name from server

    var text = new fabric.Text(randomName, {  originX: 'center',
					      originY: 'bottom',
					      fontSize: 15 });

    var group = new fabric.Group([ text, healthbar ], {
	left: 420,
	top: 420
    });

    canvas.add(group);

    return group;
}


let group = await GenerateHeroObjects();

// // create a rectangle object
// var healthbar = new fabric.Rect({
//     fill: 'green',
//     width: 50,
//     height: 4,
//     name: 'hero_healthbar',
//     originX: 'center',
//     originY: 'top'    
// });

async function GetRandomName () {
    let response = await fetch('/home/name');
    let Name = await response.json();
    return Name.name;
}

// let randomName = await GetRandomName();

// var text = new fabric.Text(randomName, {  originX: 'center',
// 					  originY: 'bottom',
// 					  fontSize: 15 });

// var group = new fabric.Group([ text, healthbar ], {
//   left: 420,
//   top: 420
// });

// canvas.add(group);

canvas.on('mouse:down', function(options) {
    var hero = GetHero();

    if(hero == null)
	return;
    

    var click_x = options.pointer.x + this.vptCoords.bl.x;
    var click_y = options.pointer.y;

    rect.set({top: hero.top+5, left: hero.left+2, original_x: click_x, original_y: click_y});
    canvas.add(rect);

    // var local_x  = rect.left - click_x;
    // var local_y  = rect.top - click_y;

    // var riseoverrun = local_y / local_x;

    // var max_x = this.vptCoords.tr.x;
    // var max_y = this.vptCoords.tr.y;
    
    // var b = local_y / ( riseoverrun * local_x );

    // var calc_y = riseoverrun * max_x + b;
    // var calc_x = (calc_y - b) / riseoverrun;

    // debugger;
    
    rect.animate({left: click_x, top: click_y}, 1000);
    
    // if (options.target) {
    // 	console.log('an object was clicked! ', options.target.type);
    // 	console.log(`object x: ${options.target.left} y: ${options.target.top}`);
    // } else {
    // 	console.log('canvas clicked!');
    // }
});

document.onkeydown = function(e) {
    var hero = GetHero();

    if(hero == null)
	return;
    
    switch (e.keyCode) {
    case 65:
	console.log('a');
	hero.animate({left:'-=25'});
	break;
    case 83:
	console.log('s');
	break;
    case 68:
	console.log('d');
	hero.animate({left:'+=35'});	
	break;
    case 87:
	console.log('w');
	break;
    default:
	console.log(e.keyCode);
    }
}

setInterval(RenderTimer, 33);
setInterval(EnemyMovement, 1000);

function EnemyMovement()
{
    var demons = GetSprites('demon');
    if(demons.length == 0)
    {
	fabric.Sprite.fromURL('/assets/wing_demon.png', createSprite(200,200),{name: 'demon', hitpoints: 100});	
    }
    demons.forEach(DemonWork);
}

function GetHero(type = 'cowboy') {
    var objects = canvas.getObjects();
    var hero = null;
    for (const element of objects) {
	switch(element.name) {
	case type:
	    hero = element;
	    break;
	}
    }
    return hero;
}    

function GetSprites(type) {
    var objects = canvas.getObjects();
    var data = [];

    for (const element of objects) {
	switch(element.name) {
	case type:
	    data.push(element);
	    break;
	}
    }
    return data;
}

function Collision () {
    let hero = GetHero();

    if(hero == null)
	return;
    var demons = GetSprites('demon');
    var bullets = GetSprites('bullet');
    for(const bullet of bullets)
    {
	for(const demon of demons)
	{
	    if (bullet.left < demon.left + demon.width  &&
		bullet.left + bullet.width > demon.left &&
		bullet.top < demon.top + demon.height   &&
		bullet.top + bullet.height > demon.top)
	    {
		demon.hitpoints -= 10;
		canvas.remove(bullet);
		console.log('hit');
	    }
	}
	if (bullet.original_x == bullet.left && bullet.original_y == bullet.top)
	{
	    canvas.remove(bullet);
	    console.log('nothing hit');
	}
    }

    for (let demon of demons)
    {
	if (demon.hitpoints <= 0)
	{
	    canvas.remove(demon);
	    console.log('DEMON killed!');	    
	}

	if (demon.left < hero.left + hero.width  &&
	    demon.left + demon.width > hero.left &&
	    demon.top < hero.top + hero.height   &&
	    demon.top + demon.height > hero.top)
	{
	    hero.hitpoints -= 10;
	    group.item(1).set({width: group.item(1).width - 1});
	    console.log('HERO hit!');

	    document.getElementById("healthbar").innerHTML = hero.hitpoints;
	}
    }
}

function DemonWork (element)
{
    var x = element.left;
    var y = element.top;

    // canvas relative x coordiante
    var tl = canvas.vptCoords.tl;

    if(x < tl.x)
    {
	var delta_x = tl.x + 500;
	element.set({left: delta_x ,top: 400});
    }
    else
    {
	var plusOrMinus = Math.random() < 0.5 ? -1 : 1;
	var rand = Math.floor(Math.random() * 50) + 1;				    		
	switch(plusOrMinus)
	{
	    case 1:
	    y += rand;
	    break;
	    case -1:
	    y -= rand;
	    break;
	}
	element.animate({left:'-=100',top: y},{duration:1000,easing: fabric.util.ease.easeOutSin});
    }
}


async function HeroCheck()
{
    let hero = GetHero();

    if(hero != null)
    {
	if(hero.hitpoints <= 0)
	{
	    canvas.remove(group);
	    canvas.remove(hero);

	    group = await GenerateHeroObjects();

	    var tl = canvas.vptCoords.tl;
	    
	    await fabric.Sprite.fromURL('/assets/cowboy.png', createSprite(tl.x+450,450),{name: 'cowboy',hitpoints: 500});
	    document.getElementById("healthbar").innerHTML = '500';	    
	}
	else
	{
	    group.set({top: hero.top - 40, left: hero.left});	    
	}

    }
}

function RenderTimer() {
    Collision();
    HeroCheck();

    var delta = new fabric.Point(-1,0);    
    canvas.relativePan(delta);
    canvas.renderAll();
} 
