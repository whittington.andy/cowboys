I wanted to try out a new rails 7 project that is covered a bit here:

[Rails 7](https://www.youtube.com/watch?v=mpWFrUwAN88)

I had always wanted to create a side scroller and I had seen fabric.js before
so I thought I would give it a try.

I actually got a basic js library to work in other test projects and thought
that it would be relativly simple to just straight import it.

Ran into this:

[ES6 import](https://stackoverflow.com/questions/57715326/fabric-js-as-es6-import-in-chrome-doesnt-work)

So I downloaded the library from:

[Fabric.js build](http://fabricjs.com/build/)

and just used the defaults.

I hacked the library till I got the global redeclartion out of it.

Then used an import command at the top of app/javascript/application.js

and imported the library locally.

You can do simple things with a canvas tag and some javascript like so:

[Fabric.js part one](http://fabricjs.com/fabric-intro-part-1)

The programmers have a little demo for animated sprites:

[Sprites](http://fabricjs.com/animated-sprite)

So I took that apart and added a background via the [gimp](https://www.gimp.org/) and my best Bob Ross.

I add some complexity to the scene adding a healthbar and I wanted to add a name.

At first I added the faker library to the front end... that was +3mb for the js lib.

No good, so I added the gem to the back-end and made a simple controller call that returns a 

name via json.

Then added some bad math to the 'demon' thingy and set it loose.

Added a cowboy and a rect 'bullet' then did some collision detection, sprinkling some health subtraction in.

Added a grid to the side of the canvas via tailwind and a little health ticker.

Working side scroller that needs some serious TLC.
